#!/usr/bin/env bash

set -euo pipefail

##### Load external configuration file
. api.config

##### Constants
BASE_API_PATH="YamahaExtendedControl/v1"

REQUIRED_TOOLS=("curl" "jq")
CURL="curl --silent"
PARSE_JSON="jq --raw-output"

# HTTP API - Get information

# Get Available Device Features /system/getFeatures
# Get Device info /system/  
# Get Network Status /system/getNetworkStatus
# Get Function Status (e.g.: Auto Power Standby) /system/getFuncStatus
# Get Location info and zone list (device) /system/getLocationInfo
# Get zone info (device|zone) /main/getStatus
# Get Sound Program List (device|zone) /main/getSoundProgramList


GET_DEVICE_INFO="system/getDeviceInfo"
GET_AVAILABLE_DEVICE_FEATURES="system/getFeatures"
GET_NETWORK_STATUS="system/getNetworkStatus"
GET_FUNCTION_STATUS="system/getFuncStatus"
GET_LOCATION_INFO_AND_ZONE_LIST="system/getLocationInfo"
GET_ZONE_INFO="main/getStatus"
GET_SOUND_PROGRAM_LIST="main/getSoundProgramList"

# HTTP API - Power Functions

# Enable/Disable Auto Power Standby 
#   /system/setAutoPowerStandby?enable=true
#   /system/setAutoPowerStandby?enable=false
# Power on /main/setPower?power=on
# Standby /main/setPower?power=standby
# Power Toggle /main/setPower?power=toggle

SET_AUTO_POWER_STANDBY_STATE="system/setAutoPowerStandby?enable="
SET_POWER_STATE="main/setPower?power="

# HTTP API - Sleep Timer

# Set Sleep timer for 60 minutes /main/setSleep?sleep=60
# Cancel Sleep timer /main/setSleep?sleep=0

SET_SLEEP_TIMER="main/setSleep?sleep="

# HTTP API - Input and Volume

# Input: Net Radio http://192.168.5.219/YamahaExtendedControl/v1/main/setInput?input=net_radio

SET_INPUT="main/setInput?input="

##### Functions

check_api_reachable()
{
    if `curl --output /dev/null --silent --fail --connect-timeout 2.0 ${DEVICE_IP/$BASE_API_PATH}`; then
        return 0
    else
        printf "ERROR: Connection to API failed.\n"
        exit 1  
    fi
}

check_required_tools()
{
    for tool in ${REQUIRED_TOOLS[*]}; do
        if ! [ `command -v ${tool}` ]; then
            echo "$tool could not be found"
            exit 1
        fi
    done
}

check_api_response_code()
{
    command_output=$1
    if [ `echo ${command_output} | $PARSE_JSON '.response_code'` -ne "0" ]; then
        echo "Not OK!"
        exit 1
    else
        echo "OK!"
        exit 0
    fi
}

run_api_get_command()
{
    $CURL $DEVICE_IP/$BASE_API_PATH/$1
}

run_api_post_command()
{
    command=`$CURL $DEVICE_IP/$BASE_API_PATH/$1`
    check_api_response_code $command
}

get_system_version()
{
    printf `run_api_get_command ${GET_DEVICE_INFO} | jq '.system_version'`
}

get_zone_info()
{
    zone_info_output=`run_api_get_command ${GET_ZONE_INFO}`
    printf "Power:\t\t$(echo $zone_info_output | $PARSE_JSON '.power' | sed 's/on/On/g; s/standby/Standby/g')\n"
    printf "Input:\t\t$(echo $zone_info_output | $PARSE_JSON '.input')\n"
    printf "Volume:\t\t$(echo $zone_info_output | $PARSE_JSON '.volume') ($(echo $zone_info_output | $PARSE_JSON '.actual_volume.value') $(echo $zone_info_output | $PARSE_JSON '.actual_volume.unit'))\n"
}

get_input()
{
    zone_info_output=`run_api_get_command ${GET_ZONE_INFO}`
    printf "Input:\t\t$(echo $zone_info_output | $PARSE_JSON '.input')\n"
}

get_device_info()
{
    device_info_output=`run_api_get_command ${GET_DEVICE_INFO}`
    network_status_output=`run_api_get_command ${GET_NETWORK_STATUS}`
    printf "Model name:\t\t$(echo $device_info_output | $PARSE_JSON '.model_name')\n"
    printf "Destination:\t\t$(echo $device_info_output | $PARSE_JSON '.destination')\n"
    printf "Device ID:\t\t$(echo $device_info_output | $PARSE_JSON '.device_id')\n"
    printf "System ID:\t\t$(echo $device_info_output | $PARSE_JSON '.system_id')\n"
    printf "System version:\t\t$(echo $device_info_output | $PARSE_JSON '.system_version')\n"
    printf "API version:\t\t$(echo $device_info_output | $PARSE_JSON '.api_version')\n"
    printf "\n"

    printf "Network name:\t\t$(echo $network_status_output | $PARSE_JSON '.network_name')\n"
    printf "Connection:\t\t$(echo $network_status_output | $PARSE_JSON '.connection')\n"
    printf "IP-Address:\t\t$(echo $network_status_output | $PARSE_JSON '.ip_address')\n"
    printf "DHCP:\t\t\t$(echo $network_status_output | $PARSE_JSON '.dhcp' | sed 's/true/Yes/g; s/false/No/g')\n"
    printf "Subnet Mask:\t\t$(echo $network_status_output | $PARSE_JSON '.subnet_mask')\n"
    printf "Default Gateway:\t$(echo $network_status_output | $PARSE_JSON '.default_gateway')\n"
    printf "DNS Server 1:\t\t$(echo $network_status_output | $PARSE_JSON '.dns_server_1')\n"
    printf "DNS Server 2:\t\t$(echo $network_status_output | $PARSE_JSON '.dns_server_2')\n"
    printf "Wireless SSID:\t\t$(echo $network_status_output | $PARSE_JSON '.wireless_lan.ssid')\n"
    printf "Wireless strength:\t$(echo $network_status_output | $PARSE_JSON '.wireless_lan.strength')\n"
    printf "Wireless type:\t\t$(echo $network_status_output | $PARSE_JSON '.wireless_lan.type')\n"
    printf "Wireless channel:\t$(echo $network_status_output | $PARSE_JSON '.wireless_lan.ch')\n"
    printf "MAC-Address:\t\t$(echo $network_status_output | $PARSE_JSON '.wireless_lan.ssid')\n"
    printf "MAC-Airplay PIN:\t$(echo $network_status_output | $PARSE_JSON '.airplay_pin')\n"

    }

get_power_status()
{
    command_output=`run_api_get_command ${GET_ZONE_INFO} | $PARSE_JSON '.power'`
    if [  $command_output == "on" ]; then
        printf "Power Status:\t\tON\n"
    elif [  $command_output == "standby" ]; then
        printf "Power Status:\t\tStandby\n"
    else
       printf "Power Status:\t\tUnknown ($command_output)"
    fi
}

set_power_state()
{
    argument=$1
    case $argument in
        on)
            run_api_post_command "${SET_POWER_STATE}on"
            ;;
        off|standby)
            run_api_post_command "${SET_POWER_STATE}standby"
            ;;
        toggle)
            run_api_post_command "${SET_POWER_STATE}toggle"
            ;;
        *)
            echo $"Usage: $0 --power {on|standby|off|toggle}"
            exit 1
    esac
}

set_input()
{
    argument=$1
    case $argument in
        "napster")
            run_api_post_command "${SET_INPUT}napster"
            ;;
        "spotify")
            run_api_post_command "${SET_INPUT}spotify"
            ;;
        "juke")
            run_api_post_command "${SET_INPUT}juke"
            ;;
        "qobuz")
            run_api_post_command "${SET_INPUT}qobuz"
            ;;
        "tidal")
            run_api_post_command "${SET_INPUT}tidal"
            ;;
        "deezer")
            run_api_post_command "${SET_INPUT}deezer"
            ;;
        "airplay")
            run_api_post_command "${SET_INPUT}airplay"
            ;;
        "mc_link")
            run_api_post_command "${SET_INPUT}mc_link"
            ;;
        "server")
            run_api_post_command "${SET_INPUT}server"
            ;;
        "net_radio")
            run_api_post_command "${SET_INPUT}net_radio"
            ;;
        "bluetooth")
            run_api_post_command "${SET_INPUT}bluetooth"
            ;;
        "usb")
            run_api_post_command "${SET_INPUT}usb"
            ;;
        "optical")
            run_api_post_command "${SET_INPUT}optical"
            ;;
        "aux")
            run_api_post_command "${SET_INPUT}aux"
            ;;
        *)
            echo $"Usage: $0 --input {aux|optical|usb|bluetooth|net_radio|server|mc_link|airplay|spotify|deezer|tidal|qobuz|juke|napster}"
            exit 1
        esac
}

##### Argument parsing

check_required_tools

case $1 in
    ""|"--help")
        command=`basename $0`
        printf "Usage:\n"
        printf "`basename $0`\n"
        printf "\t\t--version \tGet system version information\n"
        printf "\t\t--input\t\tGet and set input\n"
        printf "\t\t--power\t\tGet and set system power status\n"
        printf "\t\t--info\t\tGet system information\n"
        printf "\t\t--zone\t\tGet zone information\n"
        printf "\t\t--help\t\tThis help\n"
        ;;
    "--version")
        if check_api_reachable; then
            get_system_version
        fi
        ;;
    "--power")
        if check_api_reachable; then
            if [ "$#" -eq 2 ]; then
                set_power_state $2
            elif [ "$#" -ne 1 ]; then
                echo "$@ not implemented"
            else
                get_power_status
            fi
        fi
        ;;
    "--info")
        if check_api_reachable; then
            get_device_info
        fi
        ;;
    "--input")
        if check_api_reachable; then
            if [ "$#" -eq 2 ]; then
                set_input $2
            else
                get_input
            fi 
        fi
        ;;   
    "--zone")
        if check_api_reachable; then
            get_zone_info
        fi
        ;;
esac